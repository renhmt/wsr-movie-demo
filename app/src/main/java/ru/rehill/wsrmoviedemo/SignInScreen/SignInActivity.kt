package ru.rehill.wsrmoviedemo.SignInScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.rehill.wsrmoviedemo.MainScreen.MainActivity
import ru.rehill.wsrmoviedemo.SignUpScreen.SignUpActivity
import ru.rehill.wsrmoviedemo.common.ErrorDialog
import ru.rehill.wsrmoviedemo.common.GlobalHelper
import ru.rehill.wsrmoviedemo.databinding.ActivitySigninBinding

// класс окна SignInScreen Входа
class SignInActivity : AppCompatActivity() {

    // подключаем биндинг
    private lateinit var binding: ActivitySigninBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // подключаем биндинг
        binding = ActivitySigninBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // прослушиватель на кнопку вход
        binding.btnSignIn.setOnClickListener {
            // создаем переменные для 2х полей логина/пароля
            val userEmail = binding.email.editText?.text.toString()
            val userPass = binding.password.editText?.text.toString()

            // проверка поля на пустоту
            if (userEmail.isEmpty()){
                showError("Поле \"E-mail\" не должно быть пустым!")
            }
            // проверка поля на пустоту
            else if (userPass.isEmpty()){
                showError("Поле \"Пароль\" не должно быть пустым!")
            }
            // поиск совпадений email по форме регулярного выражения
            else if (GlobalHelper.regexEmail.find(userEmail) == null){
                showError("Некорректный E-mail!")
            }else{
                // если все выше лож то вызываем метод аутентификации
                loginUser(userEmail, userPass)
            }

        }
        // по нажатии кнопки регистрация переходим на соответствующие окно
        binding.btnSignUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

    }

    // метод аутентификации
    private fun loginUser(email: String, password: String){
        // запускаем новый фоновый поток
        CoroutineScope(Dispatchers.IO).launch {
            // вызываем метод из класса помощник для отправки запроса на сервер
            val isSuccess = GlobalHelper.loginUser(email, password, this@SignInActivity)
            // запускаем поток андроид для работы со view элементами
            CoroutineScope(Dispatchers.Main).launch {
                // если результат запроса сервера был успешный то переходим на Main Screen
                if (isSuccess){
                    val intent = Intent(this@SignInActivity, MainActivity::class.java)
                    startActivity(intent)
                    // если сервер прислал ошибку выводим диалоговое окно
                }else{
                    showError("Проблема аутентификации!")
                }
            }

        }
    }

    // метод вызова диалогового окна
    private fun showError(message: String){
        // создаем экземпляр класса
        val dialog = ErrorDialog()
        // создаем посылку
        val bundle = Bundle()
        // Кладем в посылку сообщения для контента dialog
        bundle.putString("message", message)
        // прикрепляем посылку ккласу в качестве arguments
        dialog.arguments = bundle
        // вызываем метод отображение dialog
        dialog.show(supportFragmentManager, "ErrorDialog")
    }

}