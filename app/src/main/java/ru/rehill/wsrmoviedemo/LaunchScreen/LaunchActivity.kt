package ru.rehill.wsrmoviedemo.LaunchScreen

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.WindowManager
import ru.rehill.wsrmoviedemo.R
import ru.rehill.wsrmoviedemo.SignInScreen.SignInActivity
import ru.rehill.wsrmoviedemo.SignUpScreen.SignUpActivity
import ru.rehill.wsrmoviedemo.common.GlobalHelper

//  класс окна LaunchScreen Заставки
class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        // установка окна на полный экран
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
        )
        // получаем настройки
        val sp = getSharedPreferences(GlobalHelper.appPreferencesKey, Context.MODE_PRIVATE)
        Log.i("GET_TOKEN_LAUNCH", sp.getString(GlobalHelper.appPreferencesToken, "").toString())
        Log.i("GET_TOKEN_FIRST", sp.getBoolean(GlobalHelper.appPreferencesFirst, false).toString())
        // создание отложенного вызова 3000 миллисекунд
        Handler(Looper.getMainLooper()).postDelayed(
            {
                // проверяем есть ли ключ в настройках по умолчанию false
                if (sp.getBoolean(GlobalHelper.appPreferencesFirst, false)){
                    // если есть вызываем SignInActivity
                    val intent = Intent(this, SignInActivity::class.java)
                    startActivity(intent)
                }else{
                    // если нет ключа вызываем SignUpActivity
                    val intent = Intent(this, SignUpActivity::class.java)
                    startActivity(intent)
                    // и заносим истину в настройки первого запуска
                    sp.edit().putBoolean(GlobalHelper.appPreferencesFirst, true).apply()
                }

            },
            3000
        )

    }
}