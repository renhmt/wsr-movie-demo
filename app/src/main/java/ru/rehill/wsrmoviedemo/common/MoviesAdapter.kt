package ru.rehill.wsrmoviedemo.common

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Dimension
import ru.rehill.wsrmoviedemo.MainScreen.MainActivity
import ru.rehill.wsrmoviedemo.R
import ru.rehill.wsrmoviedemo.common.modal.ItemMovie
import ru.rehill.wsrmoviedemo.databinding.MovieItemBinding

class MoviesAdapter(var movieList: List<ItemMovie>, val itemType: Int = 0) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    var context: Context? = null

    inner class ViewHolder(var binding: MovieItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        if (itemType == 1){
            binding.movieView.updateLayoutParams {
                width = parent.context.resources.getDimensionPixelSize(R.dimen.movie_item_type_1)
            }
        }
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.movieView.load(GlobalHelper.baseImageUrl+movieList[position].poster)
        holder.binding.root.setOnClickListener {
            val intent = Intent(context, MainActivity::class.java)
            val bundle = Bundle()
            bundle.putString(GlobalHelper.BUNDLE_MOVIE_ID, movieList[position].movieId)
            intent.putExtras(bundle)
            context?.startActivity(intent)
        }
    }

    override fun getItemCount() = movieList.size

}