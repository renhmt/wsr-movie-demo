package ru.rehill.wsrmoviedemo.common.modal

import kotlinx.serialization.Serializable

@Serializable
data class ItemCover(
    val backgroundImage: String,
    val foregroundImage: String,
    val movieId: String
)