package ru.rehill.wsrmoviedemo.common.modal

import kotlinx.serialization.Serializable

@Serializable
data class ItemMovie(
    val age: String,
    val description: String,
    val images: List<String>,
    val movieId: String,
    val name: String,
    val poster: String,
    val tags: List<Tag>
)

@Serializable
data class Tag(
    val idTags: String,
    val tagName: String
)