package ru.rehill.wsrmoviedemo.common

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

// класс error диалог
class ErrorDialog : DialogFragment() {

    // создаем диалог в onCreateDialog
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity.let {
            val builder = AlertDialog.Builder(it)
            builder
                // заголовок
                .setTitle("Ошибка!")
                // контент
                .setMessage(arguments?.getString("message") ?: "Ошибка")
                // кнопка
                .setNegativeButton("Закрыть"){ d, _w ->
                    d.cancel()
                }
            builder.create()
        }
    }

}