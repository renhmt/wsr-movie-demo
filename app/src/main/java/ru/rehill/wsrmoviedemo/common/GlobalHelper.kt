package ru.rehill.wsrmoviedemo.common

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.decodeFromString
import ru.rehill.wsrmoviedemo.common.modal.ItemCover
import ru.rehill.wsrmoviedemo.common.modal.ItemMovie
import java.lang.Exception

// глобальный класс помощник
class GlobalHelper {
    // объекты которые будут доступны без создания экземпляра класса
    companion object {
        // токен аутентификации
        var userToken = ""
        // ссылка на базовую страницу сайта с api
        const val baseUrl = "http://cinema.areas.su"
        const val baseImageUrl = "http://cinema.areas.su/up/images/"
        const val baseVideoUrl = "http://cinema.areas.su/up/video/"
        // конкретные ссылки на основе базы
        const val loginUrl = "$baseUrl/auth/login"
        const val regUrl = "$baseUrl/auth/register"
        const val coverUrl = "$baseUrl/movies/cover"
        const val moviesUrl = "$baseUrl/movies"
        // ключ группы настроек
        const val appPreferencesKey = "app_settings"
        // ключ настроек сохранения токена
        const val appPreferencesToken = "user_token"
        // ключ настроек сохранения первого запуска
        const val appPreferencesFirst = "user_open_first"
        // Ключи передачи
        const val BUNDLE_MOVIE_ID = "MOVIE_ID_KEY"

        val filterMoviesKey = arrayOf("inTrend", "new", "forMe")

        // создание регулярного выражения для проверки email по форме
        // где [a-z0-9] - от a до z и 0 - 9 (перечисляем доступные символы)
        // где {2,3} - количество символов
        val regexEmail = Regex("""[a-z0-9]+@[a-z0-9]+\.[a-z]{2,3}$""")

        // метод регистрации
        suspend fun registrationUser(email: String?, password: String?,
                                     firstName: String?, lastName: String?): Boolean{
            Log.i("HTTP_REG", "start")
            // переменная со статусом ошибки по умолчанию
            var status = HttpStatusCode.GatewayTimeout
            try {
                // создание HTTP запроса на основе API
                val response = HttpClient().request<HttpResponse> {
                    url(regUrl)
                    method = HttpMethod.Post
                    parameter("email", email)
                    parameter("password", password)
                    parameter("firstName", firstName)
                    parameter("lastName", lastName)
                }
                // получаем результат запроса и меняем статус
                status = response.status
            } catch (e: Exception){
                Log.e("HTTP_REG", e.toString())
            }
            // возвращаем да/нет в зависимости от совпадения статуса
            return status == HttpStatusCode.Created
        }

        suspend fun loginUser(email: String?, password: String?, context: Context): Boolean{
            Log.i("HTTP_LOGIN", "start")
            // переменная со статусом ошибки по умолчанию
            var status = HttpStatusCode.GatewayTimeout
            try {
                // создание HTTP запроса на основе API
                val response = HttpClient().request<HttpResponse> {
                    url(loginUrl)
                    method = HttpMethod.Post
                    parameter("email", email)
                    parameter("password", password)
                }
                // получаем результат запроса и меняем статус
                status = response.status
                // проверка на успех статуса 200
                if (status == HttpStatusCode.OK){
                    // парсим полученный токен и заносим в переменную
                    userToken = Json.parseToJsonElement(response.readText()).jsonObject["token"].toString()
                    // получаем настройки по ключу
                    val sp = context.getSharedPreferences(appPreferencesKey, Context.MODE_PRIVATE)
                    // записываем ключ токена в настройки
                    sp.edit().putString(appPreferencesToken, userToken).apply()
                }
                Log.i("TOKEN", userToken)
            } catch (e: Exception){
                Log.e("HTTP_LOGIN", e.toString())
            }
            // возвращаем да/нет в зависимости от совпадения статуса
            return status == HttpStatusCode.OK
        }

        suspend fun getCover(): ItemCover? {
            Log.i("HTTP_COVER", "start")
            // переменная со статусом ошибки по умолчанию
            var item: ItemCover? = null
            try {
                // создание HTTP запроса на основе API
                val response = HttpClient().request<HttpResponse> {
                    url(coverUrl)
                    method = HttpMethod.Get
                }
                // проверка на успех статуса 200
                if (response.status == HttpStatusCode.OK){
                    // парсим полученный cover
                    item = Json{ ignoreUnknownKeys = true }.decodeFromString<ItemCover>(response.readText())
                }
                Log.i("COVER", item.toString())
            } catch (e: Exception){
                Log.e("HTTP_COVER", e.toString())
            }
            // возвращаем
            return item
        }

        suspend fun getMovies(filter: String = "new"): List<ItemMovie> {
            Log.i("HTTP_MOVIES_$filter", "start")
            // переменная со статусом ошибки по умолчанию
            var item: List<ItemMovie> = listOf()
            try {
                // создание HTTP запроса на основе API
                val response = HttpClient().request<HttpResponse> {
                    url(moviesUrl)
                    parameter("filter", filter)
                    method = HttpMethod.Get
                }
                // проверка на успех статуса 200
                if (response.status == HttpStatusCode.OK){
                    // парсим полученный cover
                    item = Json{ ignoreUnknownKeys = true }.decodeFromString<List<ItemMovie>>(response.readText())
                }
                Log.i("MOVIES_$filter", item.toString())
            } catch (e: Exception){
                Log.e("HTTP_MOVIES_$filter", e.toString())
            }
            // возвращаем
            return item
        }

    }
}