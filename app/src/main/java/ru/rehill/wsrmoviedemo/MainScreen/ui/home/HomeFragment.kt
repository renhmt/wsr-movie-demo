package ru.rehill.wsrmoviedemo.MainScreen.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import coil.load
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.rehill.wsrmoviedemo.MainScreen.MainActivity
import ru.rehill.wsrmoviedemo.common.GlobalHelper
import ru.rehill.wsrmoviedemo.common.MoviesAdapter
import ru.rehill.wsrmoviedemo.common.modal.ItemMovie
import ru.rehill.wsrmoviedemo.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private var filterList: List<RecyclerView> = listOf()
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        filterList = listOf(
            binding.inTrendList,
            binding.newList,
            binding.forMeList
        )
        loadContent()

        return root
    }

    // метод аутентификации
    private fun loadContent(){
        // запускаем новый фоновый поток
        CoroutineScope(Dispatchers.IO).launch {
            // вызываем метод из класса помощник для отправки запроса на сервер
            val cover = GlobalHelper.getCover()
            val filterMovies = mutableListOf<List<ItemMovie>>()
            GlobalHelper.filterMoviesKey.forEach {
                filterMovies += GlobalHelper.getMovies(it)
            }
            // запускаем поток андроид для работы со view элементами
            CoroutineScope(Dispatchers.Main).launch {
                // если результат запроса сервера был успешный то отображаем cover
                if (cover != null){
                    binding.coverImg.load(GlobalHelper.baseImageUrl + cover.foregroundImage)
                    binding.coverBtn.setOnClickListener {
                        val intent = Intent(context, MainActivity::class.java)
                        val bundle = Bundle()
                        bundle.putString(GlobalHelper.BUNDLE_MOVIE_ID, cover.movieId)
                        intent.putExtras(bundle)
                        startActivity(intent)
                    }
                }
                for (i in 0..filterMovies.lastIndex) {
                    filterList[i].adapter = MoviesAdapter(filterMovies[i], i)
                }
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}