package ru.rehill.wsrmoviedemo.SignUpScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.rehill.wsrmoviedemo.MainScreen.MainActivity
import ru.rehill.wsrmoviedemo.SignInScreen.SignInActivity
import ru.rehill.wsrmoviedemo.common.ErrorDialog
import ru.rehill.wsrmoviedemo.common.GlobalHelper
import ru.rehill.wsrmoviedemo.databinding.ActivitySignupBinding

// класс окна SignUpScreen Регистрации
class SignUpActivity : AppCompatActivity(), View.OnFocusChangeListener {

    // viewBinding
    private lateinit var binding: ActivitySignupBinding
    // создаем мап с отложенной инициализацией
    private lateinit var arrayBox: MutableMap<EditText, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // viewBinding
        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // инициализируем мап, где ключ - EditText, значение - String
        arrayBox = mutableMapOf<EditText, String>(
            binding.firstName to "",
            binding.lastName to "",
            binding.email to "",
            binding.password to "",
            binding.password2 to "",
        )

        // проходимся циклом по списку ключей из мап и подключим этим полям слушитель фокуса
        for ((key, _value) in arrayBox){
            key.onFocusChangeListener = this
        }

        binding.btnSignUp.setOnClickListener {
            // тк по нажатию кнопки фокус не меняется мы вызываем переключение фокуса на кнопку
            binding.btnSignUp.requestFocusFromTouch()
            // поиск по мап на совпадения значения (проверку на пустоту)
            if (arrayBox.containsValue("")){
                showError("Поля не должны быть пустыми!")
            }
            // поиск совпадений email по форме регулярного выражения
            else if (GlobalHelper.regexEmail.find(arrayBox[binding.email].toString()) == null){
                showError("Некорректный E-mail!")
            }
            // проверка на совпадение 2х полей с паролем
            else if (arrayBox[binding.password] != arrayBox[binding.password2]){
                showError("Пароли должны совпадать!")
            // если все выше лож то вызываем метод регистрации
            }else{
                regUser()
            }
        }
        // по нажатии кнопки входа переходим на соответствующие окно
        binding.btnSignIn.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }
    }

    // если сработает смена фокуса у прослушивымаих элементов
    override fun onFocusChange(v: View?, _hasFocus: Boolean) {
        // заменим значение в мап на значение у элемента
        arrayBox[v as EditText] = v.text.toString()
    }

    // метод для регистрации пользователя
    private fun regUser(){
        // запускаем новый фоновый поток
        CoroutineScope(Dispatchers.IO).launch {
            // вызываем метод из класса помощник для отправки запроса на сервер
            var isSuccess = GlobalHelper.registrationUser(
                arrayBox[binding.email],
                arrayBox[binding.password],
                arrayBox[binding.firstName],
                arrayBox[binding.lastName]
            )
            // если успех результат с сервера вызываем метод аутентификации с теми же значениями
            if (isSuccess){
                Log.i("TOKEN", "start")
                isSuccess = GlobalHelper.loginUser(
                        arrayBox[binding.email],
                        arrayBox[binding.password],
                        this@SignUpActivity
                    )
            }
            // возвращаемся в основной поток для отображения результата
            CoroutineScope(Dispatchers.Main).launch {
                // если все ок то переходим на Main Screen
                if (isSuccess){
                    val intent = Intent(this@SignUpActivity, MainActivity::class.java)
                    startActivity(intent)
                // если нет то отображаем диалоговое окно с ошибкой
                }else{
                    showError("Ошибка сервера!")
                }
            }

        }
    }

    // метод вызова диалогового окна
    private fun showError(message: String){
        // создаем экземпляр класса
        val dialog = ErrorDialog()
        // создаем посылку
        val bundle = Bundle()
        // Кладем в посылку сообщения для контента dialog
        bundle.putString("message", message)
        // прикрепляем посылку ккласу в качестве arguments
        dialog.arguments = bundle
        // вызываем метод отображение dialog
        dialog.show(supportFragmentManager, "ErrorDialog")
    }

}